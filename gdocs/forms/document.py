#-*- coding: utf8 -*-

from django import forms

from ..models import Document, Permissions

class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ['name','id']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'id': forms.HiddenInput(),
        }
        exclude=['remote_id']

PermissionFormSet = forms.inlineformset_factory(Document, Permissions,
                            fields=['user', 'permission', 'id'],
                            widgets={'user': forms.widgets.Select(attrs={'class': 'form-control'}),
                                     'permission': forms.widgets.Select(attrs={'class': 'form-control'}),
                                     'id': forms.HiddenInput(),
                                     },
                            extra=5, max_num=5, exclude=['remote_id']
                            )
