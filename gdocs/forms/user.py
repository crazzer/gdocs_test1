#-*- coding: utf8 -*-

from django import forms
from django.contrib.auth.forms import UserCreationForm


class MyUserCreationForm(UserCreationForm):
    """Форма создания пользователя с обязательным полем email"""
    class Meta:
        fields = ("username", "email",)
    email = forms.EmailField(required=True)