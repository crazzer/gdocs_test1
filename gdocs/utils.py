#-*- coding: utf-8 -*-

import os
import httplib2
from collections import defaultdict

from apiclient import discovery
from oauth2client.django_orm import Storage
from oauth2client import client, xsrfutil
from models import CredentialsModel

from gdocs_test1 import settings

CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), 'client_secrets.json')
FLOW = client.flow_from_clientsecrets(
    CLIENT_SECRETS,
    scope='https://www.googleapis.com/auth/drive',
    redirect_uri='http://localhost:8000/gdocs/oauth2callback')


def get_duplicates(lst):
    ''' Определение повторяющихся элементов списка
    :param lst: список для проверки
    :return: Словарь, где ключи - элементы исходного списка, повторяющиеся более 1 раза, а значения - индексы этих элементов в исходном списке
    '''
    D = defaultdict(list)
    for i, item in enumerate(lst):
        D[item].append(i)
    D = {k: v for k, v in D.items() if len(v) > 1}
    return D

def get_credential(user):
    storage = Storage(CredentialsModel, 'id', user, 'credential')
    return storage.get()

def check_oauth(credential, user):
    if credential is None or credential.invalid == True:
        FLOW.params['state'] = xsrfutil.generate_token(settings.SECRET_KEY,
                                                   user)
        authorize_url = FLOW.step1_get_authorize_url()
        return authorize_url
    return None

def get_drive_service(credential):
    http = credential.authorize(httplib2.Http())
    return discovery.build('drive', 'v2', http=http)
