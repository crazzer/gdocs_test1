// Доступно 5 групп ввода. Если заполнены не все, то одну пустую оставляем доступной, остальные скрываем.
$( "#user1" )
  .change(function () {
  if ($("#user1 select[name='permissions_set-0-user'] :selected")[0].value == "") {
    $("#user2 select[name='permissions_set-1-user'] :first").prop("selected", true);
    $("#user2").fadeOut().change();
    $("#id_permissions_set-0-DELETE").prop("checked",true);
  }
  else {
    $("#user2").fadeIn();
    $("#id_permissions_set-0-DELETE").prop("checked",false);
  }
  }).change();

$( "#user2" )
  .change(function () {
  if ($("#user2 select[name='permissions_set-1-user'] :selected")[0].value == "") {
    $("#user3 select[name='permissions_set-2-user'] :first").prop("selected", true);
    $("#user3").fadeOut().change();
    $("#id_permissions_set-1-DELETE").prop("checked",true);
  }
  else {
    $("#user3").fadeIn();
    $("#id_permissions_set-1-DELETE").prop("checked",false);
  }
  }).change();

$( "#user3" )
  .change(function () {
  if ($("#user3 select[name='permissions_set-2-user'] :selected")[0].value == "") {
    $("#user4 select[name='permissions_set-3-user'] :first").prop("selected", true);
    $("#user4").fadeOut().change();
    $("#id_permissions_set-2-DELETE").prop("checked",true);
  }
  else {
    $("#user4").fadeIn();
    $("#id_permissions_set-2-DELETE").prop("checked",false);
  }
  }).change();

$( "#user4" )
  .change(function () {
  if ($("#user4 select[name='permissions_set-3-user'] :selected")[0].value == "") {
    $("#user5 select[name='permissions_set-4-user'] :first").prop("selected", true);
    $("#user5").fadeOut();
    $("#id_permissions_set-3-DELETE").prop("checked",true);
  }
  else {
    $("#user5").fadeIn();
    $("#id_permissions_set-3-DELETE").prop("checked",false);
  }
  }).change();

$( "#user5" )
  .change(function() {
  if ($("#user4 select[name='permissions_set-3-user'] :selected")[0].value == "") {
    $("#id_permission_set-4-DELETE").prop("checked", true);
  }
  else {
    $("#id_permission_set-4-DELETE").prop("checked", false);
  }
  }).change();