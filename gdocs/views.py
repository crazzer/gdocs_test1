#-*- coding: utf8 -*-

import httplib2
import os

from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect

from oauth2client.django_orm import Storage
from oauth2client import client, xsrfutil

from apiclient import discovery
from gdocs.forms.document import PermissionFormSet

from gdocs_test1 import settings
from .models import Document, CredentialsModel
from .forms.document import DocumentForm
from bl.document import create_document, create_permissions, edit_document as bl_edit_document
from .utils import check_oauth, get_drive_service, get_credential, FLOW



@csrf_protect
@login_required()
def index(request):
    '''Отобразить все добавленные документы'''

    credential = get_credential(request.user)
    authorize_url = check_oauth(credential, request.user)
    if authorize_url: return HttpResponseRedirect(authorize_url)

    document_list = Document.objects.order_by('-updated_at')
    context = {'document_list': document_list}

    return render(request, 'gdocs/index.html', context)

@login_required
def new_document(request):
    """Добавление нового документа"""
    credential = get_credential(request.user)
    authorize_url = check_oauth(credential, request.user)
    if authorize_url: return HttpResponseRedirect(authorize_url)

    # Получение экземпляра Drive API service
    service = get_drive_service(credential)
    if request.method == 'POST':
        # Обработка введенных данных
        form = DocumentForm(request.POST)
        if form.is_valid():
            document = form.save(commit=False)
            formset = PermissionFormSet(request.POST, instance=document)
            if formset.is_valid():
                create_document(service, document)
                permissions = formset.save(commit=False)
                create_permissions(service, permissions)
                return HttpResponseRedirect('/')
        else:
            formset = PermissionFormSet(request.POST)
    else:
        # Создание пустой формы для ввода
        document = Document()
        form = DocumentForm(instance=document)
        formset = PermissionFormSet(instance=document)
    return render(request, 'gdocs/new.html', {'form': form, 'formset':formset}, context_instance=RequestContext(request))

@login_required
def edit_document(request, document_id):
    """Редактирование документа"""
    document = get_object_or_404(Document, pk=document_id)
    credential = get_credential(request.user)
    authorize_url = check_oauth(credential, request.user)
    if authorize_url:
        return HttpResponseRedirect(authorize_url)

    # Получение экземпляра Drive API service
    service = get_drive_service(credential)
    if request.method == 'POST':
        form = DocumentForm(request.POST, instance=document)
        formset = PermissionFormSet(request.POST, instance=document)
        if form.is_valid() and formset.is_valid():
            document = form.save(commit=False)
            permissions = formset.save(commit=False)
            bl_edit_document(service, document, permissions, formset.deleted_objects)
            return HttpResponseRedirect('/')
    else:
        form = DocumentForm(instance=document)
        formset = PermissionFormSet(instance=document)
    return render(request, 'gdocs/new.html', {'form': form, 'formset': formset}, context_instance=RequestContext(request))

@login_required
def remove_document(request, document_id):
    """Удаление документа.

    Удаление производится локально и не затрагивает данные в Google Drive."""
    document = get_object_or_404(Document, pk=document_id)
    document.delete()
    return HttpResponseRedirect('/')


# Authentication views
@csrf_protect
@login_required
def auth_return(request):
    # TODO: применяется ли csrf-protection из Django в данном случае, или лучше чинить validate_token?
    # if not xsrfutil.validate_token(settings.SECRET_KEY, request.REQUEST['state'],
    #                                 request.user):
    #     return  HttpResponseBadRequest()
    credential = FLOW.step2_exchange(request.REQUEST)
    storage = Storage(CredentialsModel, 'id', request.user, 'credential')
    storage.put(credential)
    return HttpResponseRedirect("/")

def login(request):
    return render(request, 'gdocs/login.html')

def logout_view(request):
    logout(request)
    return HttpResponseRedirect("/")