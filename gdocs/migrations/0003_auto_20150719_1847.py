# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gdocs', '0002_auto_20150715_0007'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='permissions',
            unique_together=set([('user', 'document')]),
        ),
    ]
