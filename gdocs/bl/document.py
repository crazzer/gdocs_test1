#-*- coding: utf8 -*-

from django.utils import timezone
from apiclient import errors

from ..models import Document, Permissions


def create_document(service, document):
    """Создание нового документа

    :param service: экземпляр Drive API service
    :param document: имя файла
    :return: None
    """
    #TODO: Возвращать информацию об исходе операции
    document_info = upload_file(service, document.name, 'Added with gdocs service')
    if document_info:
        document.url=document_info['alternateLink']
        document.remote_id=document_info['id']
        document.updated_at = timezone.now()
        document.save()
    return document_info

def create_permissions(service, permissions):
    """Сохранение набора прав, введенных в форме

    :param service: экземпляр Drive API service
    :param permissions: список экземпляров Permissions, которые необходимо сохранить
    :return:
    """
    for permission in permissions:
        create_permission(service, permission)

def create_permission(service, permission):
    """Сохранение права, введенного в форме.

    :param service: экземпляр Drive API service
    :param permission: экземплят Permissions.
    :return:
    """
    permission_info = insert_permission(service, permission.document.remote_id, permission.user.email, permission.permission)
    if permission_info:
        permission.remote_id=permission_info['id']
        permission.save()
    return permission_info

def edit_document(service, document, permissions, deleted_permissions):
    """Редактирование документа.

    :param service: экземпляр Drive API service
    :param document: информация о документе {'name': <name>, 'remote_id': <google_drive_file_id>, 'id': <local_file_id>}
    :param permissions: список словарей {'user': <User object>,
                                         'permission': <permission code>,
                                         'remote_id': <google_drive_permission_id>,
                                         'id': <local_permission_id>}
    :return:
    """
    old_document = Document.objects.get(pk=document.id)
    if document.name != old_document.name:
        patch_file(service, document.remote_id, document.name)
        document.save()

    # Обработка удаленных прав
    for permission in deleted_permissions:
        # Если пользователя нет там, где он только что был, удаляем старое право
        delete_permission(service, document.remote_id, permission.remote_id)
        permission.delete()

    # Обработка измененных/добавленных прав
    for permission in permissions:
        old_permission = Permissions.objects.get(pk=permission.id)
        if permission.user and not old_permission:
            # Если пользователя раньше не было, а теперь он есть, создаем новое право
            create_permission(service, permission)
        elif permission.user != old_permission.user:
            # Если изменился пользователь, мы должны удалить старое право и добавить новое
            delete_permission(service, document.remote_id, old_permission.remote_id)
            old_permission.delete()
            create_permission(service, permission)
        elif permission.permission != old_permission.permission:
            # Если изменилось право, то патчим старую запись
            patch_permission(service, document.remote_id, old_permission.remote_id, permission.permission)
            permission.save()

# Работа с Google Drive API
# Работа с файлами
def upload_file(service, title, description):
    '''Добавление пустого файла в Google Drive

    :param service: экземпляр Drive API service
    :param title: название файла
    :param description: описание файла
    :return: Информацию о файле в случае успеха, иначе None
    :
    '''
    body = {
        'title': title,
        'description': description,
        'mimeType': 'text/plain',
    }
    try:
        file = service.files().insert(body=body).execute()
        return file
    except errors.HttpError, error:
        print u'An error occured: %s' % error
        return None

def patch_file(service, file_id, title):
    """Изменение названия файла

    :param service: экземпляр Drive API service
    :param file_id: id изменяемого файла
    :param title: новое название файла
    :return:
    """
    file_ = {'title': title}
    try:
        updated_file = service.files().patch(fileId=file_id,
                                             body=file_,
                                             fields='title').execute()
        return updated_file
    except errors.HttpError, error:
        print 'An error occured: %s' % error

# Работа с правами
def insert_permission(service, file_id, value, role):
    """Добавление нового права

    :param service: экземпляр Drive API service
    :param file_id: id файла, к которому задаем доступ.
    :param value: email пользователя, которому предоставляется доступ
    :param role: Право. Допустимые значения 'writer' or 'reader' (также возможно значение 'owner',
                        но в нашем приложении оно не используется).
    :return: информацию об экземпляре права в случае успеха, иначе None.
    """
    new_permission = {
        'value': value,
        'type': 'user',
        'role': role
    }
    try:
        return service.permissions().insert(
            fileId=file_id, body=new_permission).execute()
    except errors.HttpError, error:
        print 'An error occurred: %s' % error
    return None

def patch_permission(service, file_id, permission_id, role):
    """Изменение права

    :param service: экземпляр Drive API service
    :param file_id: id файла, к которому задаем доступ.
    :param permission_id: id изменяемого права
    :param role: новое право
    :return:
    """
    patched_permission = {'role': role}
    try:
        return service.permissions().patch(
            fileId=file_id, permissionId=permission_id,
            body=patched_permission).execute()
    except errors.HttpError, error:
        print 'An error occurred: %s' % error
    return None

def delete_permission(service, file_id, permission_id):
    """Удаление права.

    :param service: экземпляр Drive API service.
    :param file_id: id файла, к которому задаем доступ.
    :param permission_id: id права, которое хотим удалить
    """
    try:
        service.permissions().delete(
            fileId=file_id, permissionId=permission_id).execute()
    except errors.HttpError, error:
        print 'An error occurred: %s' % error



