#-*- coding: utf8 -*-
import os

from django.test import TestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.utils import timezone
from apiclient.http import HttpMock, RequestMockBuilder
from apiclient.discovery import build
from gdocs.bl.document import create_permission

from .models import Document, Permissions
from bl.document import create_document

DATA_DIR = os.path.join(os.path.dirname(__file__), 'data')

def datafile(filename):
    return os.path.join(DATA_DIR, filename)

class TestSyncDocument(TestCase):
    fixtures = ['fixture.json']
    def test_create_document_success(self):
        document = Document(name='test')

        http = HttpMock(datafile('drive.json'), {'status': '200'})
        response = '{"alternateLink": "http://drive.google.com/file/d/123456", "id": "123456"}'
        request_mock = RequestMockBuilder({'drive.files.insert': (None, response)})
        service = build('drive', 'v2', http, requestBuilder=request_mock)

        create_document(service, document)

        self.assertEqual(document.remote_id, '123456')
        self.assertEqual(document.url, 'http://drive.google.com/file/d/123456')

    # TODO: проверка неудачного выполнения запроса
    # def test_create_document_failure(self):
    #     document = Document(name='test')
    #
    #     http = HttpMock(datafile('drive.json'), {'status': '200'})
    #
    #     response = '{}'
    #     request_mock = RequestMockBuilder({'drive.files.insert': (None, response)})
    #     service = build('drive', 'v2', http, requestBuilder=request_mock)
    #     result = create_document(service, document)
    #
    #     self.assertEqual(result, None)

    def test_create_permission(self):
        document = Document(name='test', remote_id='123456', updated_at=timezone.now())
        document.save()
        user = User(username='test_user', email='em@i.l')
        user.save()
        permission = Permissions(user=user, document=document, permission=Permissions.READER)

        http = HttpMock(datafile('drive.json'), {'status': '200'})
        response = '{"id": "654321"}'
        request_mock = RequestMockBuilder({'drive.permissions.insert': (None, response)})
        service = build('drive', 'v2', http, requestBuilder=request_mock)

        result = create_permission(service, permission)

        self.assertEqual(permission.remote_id, '654321')

class TestIndex(TestCase):
    def test_index_with_redirect(self):
        response = self.client.get(reverse('gdocs:index'))
        self.assertEqual(response.status_code, 302)

    # TODO: как из тестов попасть в ветвь, где не будет редиректов?

