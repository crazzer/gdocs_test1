from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^oauth2callback/', views.auth_return, name='oauth2callback'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^new/', views.new_document, name='new'),
    url(r'^(?P<document_id>[0-9]+)/remove/$', views.remove_document, name='remove'),
    url(r'^(?P<document_id>[0-9]+)/edit/$', views.edit_document, name='edit'),
]