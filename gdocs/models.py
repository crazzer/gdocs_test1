#-*- coding: utf8 -*-

from django.db import models
from django.contrib.auth.models import User

from oauth2client.django_orm import CredentialsField

class Document(models.Model):
    """Информация о документе"""
    updated_at = models.DateTimeField()
    name = models.CharField(max_length=200)
    url = models.CharField(max_length=500)
    remote_id = models.CharField(max_length=200)
    users = models.ManyToManyField(User, through='Permissions')

    def __unicode__(self):
        return self.name

class Permissions(models.Model):
    """Информация о правах пользователя"""
    class Meta:
        unique_together = (('user', 'document'),)
    OWNER = 'owner'
    WRITER = 'writer'
    READER = 'reader'
    PERMISSIONS_SET = (
        # (OWNER, 'Owner'),
        (READER, 'Reader'),
        (WRITER, 'Writer'),
    )
    document = models.ForeignKey(Document)
    user = models.ForeignKey(User)
    remote_id = models.CharField(max_length=200)
    permission = models.CharField(max_length=6,
                                  choices=PERMISSIONS_SET,
                                  default=READER)

class CredentialsModel(models.Model):
    id = models.ForeignKey(User, primary_key=True)
    credential = CredentialsField()